package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

const (
	BaseURL    = "https://pokeapi.co/api/v2"
	MachineURL = "https://pokeapi.co/api/v2/machine/"
)

type PokeAPIPage struct {
	count    int
	next     string
	previous string
}

type MachineObjectItem struct {
	name string
	url  string
}

type MachineObject struct {
	MachineID    int               `json:"id"`
	MachineItem  MachineObjectItem `json:"item"`
	MachineMove  MachineObjectItem `json:"move"`
	VersionGroup MachineObjectItem `json:"version_id"`
}

func GetMachineList() {
	fmt.Println("Being HTTP Request for MachineLists!")

	machineListResponse, err := http.Get(MachineURL)
	if err != nil {
		log.Fatal("Hmm... Error: ", err)
	}
	defer machineListResponse.Body.Close()

	responseBodyBytes, _ := ioutil.ReadAll(machineListResponse.Body)

	// Convert response to String
	responseBodyString := string(responseBodyBytes)
	fmt.Printf("Response as a String: %s", responseBodyString)

	// Unmarshall to Struct!
	var responseStruct PokeAPIPage
	json.Unmarshal(responseBodyBytes, &responseStruct)

	fmt.Printf("Results: %v", responseStruct.count)
	if responseStruct.count < 1 {
		log.Printf("Can't seem to get the right value: %v", responseStruct.next)
	}

}

func main() {
	// Begin HTTP Request
	httpResponse, err := http.Get(MachineURL)
	if err != nil {
		log.Fatal("Woops! Error: ", err)
	}
	defer httpResponse.Body.Close()

	// Attempt to read the content of the response
	responseBody, err := ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		log.Fatal("Error reading body: ", err)
	}

	fmt.Printf("Results are in! %s\n", responseBody)

	GetMachineList()

	// Now the Pokemon Stuff!
	//pokeResults, err := GetMachineList()
	//if err != nil {
	//	log.Fatal("Error getting Machine List.")
	//}
	//
	//fmt.Printf("Results: %v", pokeResults)
}
